package cn.uncode.springcloud.starter.bus.mq.rabbit;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by littlersmall on 16/9/5.
 */
@Slf4j
public class RetryCache {
    private boolean stop = false;
    private Map<String, MessageWithTime> map = new ConcurrentSkipListMap<>();
    private RabbitmqTemplate rabbitTemplate;

    public void setSender(RabbitmqTemplate rabbitTemplate) {
    	this.rabbitTemplate = rabbitTemplate;
        startRetry();
    }

    public void add(MessageWithTime messageWithTime) {
        map.putIfAbsent(messageWithTime.getId(), messageWithTime);
    }
    
    public void del(String id) {
        map.remove(id);
    }

    private void startRetry() {
        new Thread(() ->{
            while (!stop) {
                try {
                    Thread.sleep(Constants.RETRY_TIME_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                long now = System.currentTimeMillis();

                for (Map.Entry<String, MessageWithTime> entry : map.entrySet()) {
                    MessageWithTime messageWithTime = entry.getValue();

                    if (null != messageWithTime) {
                        if (messageWithTime.getTime() + 3 * Constants.VALID_TIME < now) {
                            log.info("send message {} failed after 3 min ", messageWithTime);
                            del(entry.getKey());
                        } else if (messageWithTime.getTime() + Constants.VALID_TIME < now) {
                        	rabbitTemplate.send(messageWithTime);
//                            DetailRes res = sender.send(messageWithTime);

//                            if (!res.isSuccess()) {
//                                log.info("retry send message failed {} errMsg {}", messageWithTime, res.getErrMsg());
//                            }
                        }
                    }
                }
            }
        }).start();
    }
}
