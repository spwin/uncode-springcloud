package cn.uncode.springcloud.starter.canary.api;

public interface CanaryContext {
	
	CanaryStrategy getCanaryStrategy();

}
